#  Frontend

En esta carpeta se encuentran los archivos necesarios para desplegar la interfaz del proyecto.

* build: Se genera al ejecutar el comando `npm run  build`
* img: Donde se guardan  las imagenes que no vienen del backend
* style: Estilos css para adornar el esquema visual
* utils: Archivos que tienen varias funcionalidades js

    
## Uso

Para poder usar este proyecto en un ambiente de *desarrollo* se debe: 

*  `npm run dev` Ejecutará un servicio con parcel que deslegará la interfaz en localhost:1234
*  `npm run build` Ejecutará la compilación "bundle" de los archivos
