// import { getClothes } from "./api.js";

var picture;
var proposals;
var proposals = [];
const API_URL = "http://localhost:5000";
var age;
var selectedGender;

window.startWebcam = function startWebcam() {
  let count = 0;

  takeSnapshot(count);

};

function takeSnapshot(count) {
  let takePictureBtn = document.getElementById("action");
  let gender = document.getElementById("gender");
  selectedGender = gender.options[gender.selectedIndex].value;
  age = document.getElementById("age").value;

  if(selectedGender === '' || age <= 0 || isNaN(age)) return

  takePictureBtn.setAttribute("disabled", true);
  takePictureBtn.textContent = (10 - count).toString();

  var snap = setInterval(() => {
    count += 1;
    takePictureBtn.textContent = (10 - count).toString();

    if (count === 5) {
      //10
      Webcam.snap(function (data_uri) {
        picture = data_uri;
      });
      // document.getElementById("snapShot").innerHTML =
      //   '<img src="' + picture + '"  />';

      takePictureBtn.removeAttribute("disabled");
      takePictureBtn.textContent = "Tomar foto";
      document.getElementById("actionPreview").removeAttribute("disabled");

      clearInterval(snap);
    }
  }, 1000);
}

window.previewImage =  async function previewImage() {
  preview = document.getElementById("imagePreview");
  if (picture !== undefined) {
    // --- LLAMADO A LA API PARA TRAER LAS IMÁGENES ---
     proposals = await getClothes(picture).then((res) => 
      res.json()
    );

    if(proposals.error){
      console.log(proposals.error)
    }
    else{
      proposals  =  proposals.response;
      document.getElementById("image-option1").src  = `${API_URL}${proposals[0]}`;
      document.getElementById("image-option2").src  = `${API_URL}${proposals[1]}`;
      document.getElementById("image-option3").src  = `${API_URL}${proposals[2]}`;
  
      preview.src = picture;
    }
  }
};


window.imageOptions = function imageOptions(option) {
  preview = document.getElementById("imagePreview");

  switch (option) {
    case 1:
      preview.src = `${API_URL}${proposals[3]}`;
      break;
    case 2:

      preview.src = `${API_URL}${proposals[4]}`;
      break;
    case 3:
      preview.src = `${API_URL}${proposals[5]}`;
      break;
    default:
      console.log("Error...");
      break;
  }
};

function getClothes(img) {
  const endpoint = `${API_URL}/getClothes`;
  const response = fetch(endpoint, {
    method: "POST",
    mode: "cors",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      base64_img: img,
      age: age.toString(),
      gender: selectedGender,
    }),
  });
  return response;
}
