#  Just the clothes

Este proyecto presenta una aplicación de inteligencia artificial que permite recomendar ropa y visualizarla en una fotografía tomada desde una página web.

## Contenido

*  Frontend
*  Backend

En estas carpetas se encuentran los diferentes módulos del proyecto, en el backend está toda la lógica que comunica los modelos con una api desarrollada en flask y python. 

En el frontend una interfaz desarrollada con html, css y js.
    
## Uso

Para poder usar este proyecto en un ambiente de *desarrollo* se debe: 

*  Clonar el repositorio
*  Ingresar a la carpeta frontend y ejecutar `npm install`
*  Ejecutar `npm run dev`
*  Ingresar a la carpeta backend y ejecutar `pip install -r requeriments/dev.txt`
*  Ejecutar `python app.py`

***Nota*** Para más información ingresar a las carpetas `frontend` y `backend`
## Have fun :D
