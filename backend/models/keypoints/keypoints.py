import mediapipe as mp
import numpy as np
import cv2

mpPose = mp.solutions.pose
pose = mpPose.Pose()

def calculate_keypoints (image_path):
    '''
    Keypoints structure: 

    Normalized depending on image size

    [{ x: 0.49346205592155457
        y: 0.09763598442077637
        z: -0.7191265225410461
        visibility: 0.9999951124191284
    }]
    '''
    img = cv2.imread(image_path)
    try:
        results = pose.process(img).pose_landmarks.landmark

        interest_landmarks = [
            8,5,2,7,10,9, #Keypoints de la cabeza
            12,11 # Hombros der, izq
        ]

        # Obtiene solo los keypoints de interés
        kps = []
        for id, lm in enumerate(results):
            if id in interest_landmarks:
                kps.append(lm)
        
        if len(kps) != 8:
            print(kps)
            return {"error": "No se pudo identificar correctamente tu cuerpo"} 

        return kps
    except:
        return {"error":"No se encontró un cuerpo"}

