import unittest
from utils.utils import allowed_file, get_clothes_images
from person.person import Person
class TestUtils(unittest.TestCase):
    
    def test_allowed_file(self):
        should_allow = allowed_file("data:image/jpg;123asfioaj",["data:image"])

        self.assertEqual(should_allow,True)

    def test_get_clothes_images_return_3_files(self):
        person = Person(age=10,gender="Male")
        proposals = get_clothes_images(person)
        self.assertEqual(len(proposals),3)
        self.assertEqual(proposals[0]["type"],"file")
        self.assertEqual(proposals[1]["type"],"file")
        self.assertEqual(proposals[2]["type"],"file")
