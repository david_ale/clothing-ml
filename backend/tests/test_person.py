import unittest
from person.person import Person

class TestPerson (unittest.TestCase):

    # [setKeypoints]
    #TODO: Define the keypoints structure
    def test_setKeypoints(self):

        keypoints = [1,2,3]
        person = Person(keypoints)

        self.assertEqual(person.keypoints,keypoints)  

        person.setKeypoints([2,3,4])
        self.assertEqual(person.keypoints,[2,3,4])  

    # [setAge] 
    def test_setAge(self):

        age = 10
        person= Person([], age)

        self.assertEqual(person.age,age)  

        person.setAge(5)
        self.assertEqual(person.age,5)  

    def test_setAge_Data_Type ( self):

        age = "20 years"
        person= Person([])

        person.setAge(age)

        self.assertEqual(person.age,None)

    # [setGender]
    def test_setGender(self):

        gender = "Male"
        person= Person([], gender=gender)

        self.assertEqual(person.gender,gender)  

        person.setGender("Female")
        self.assertEqual(person.gender,"Female")  
        
    def test_setGender_Value(self):
        gender = "Dog"
        person= Person([])

        person.setGender(gender)
        self.assertEqual(person.gender,None)  

    # [setPhoto]
    def test_setPhoto(self):
        pass
    # [calculateKeypoints]
