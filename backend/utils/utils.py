import requests
import cv2
import numpy as np
import os 
from io import BytesIO
import base64
from PIL import Image
from matplotlib import cm

TOKEN = "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"
DATASET_URL = "https://api.github.com/repos/Dalejan/clothes_dataset/contents"

# [UTILS]
def allowed_file(filename, ALLOWED_EXTENSIONS):
    return '/' in filename and \
           filename.split('/')[0].lower() in ALLOWED_EXTENSIONS

def cut_face(person):
    if(len(person.keypoints) == 6):
        raise Exception("Face not identified")
        
    kps_face = person.keypoints[:6] # Sabemos que los primeros 5 kps son los de la cara
    
    #Obtenemos las coordenadas reales en la imagen
    img = cv2.imread(person.photo)
    h, w, _ = img.shape

    real_kps_face_x=[]
    real_kps_face_y=[]
    for kp in kps_face:
        real_x = w*kp.x
        real_y = h*kp.y

        real_kps_face_x.append(real_x)
        real_kps_face_y.append(real_y)

    #Calculando el recuadro
    x_min = int(min(real_kps_face_x)*0.9)
    x_max = int(max(real_kps_face_x)*1.1)
    print(x_max - x_min)
    y_min = int(min(real_kps_face_y)*0.7)
    y_max = int(max(real_kps_face_y)*1.4)
    print(y_max - y_min)


    #Cortamos la cara de la imagen
    cropped_img = img[ y_min:y_max, x_min:x_max] 
    # cv2.imshow("img",cropped_img)
    # cv2.waitKey(0)
    return cropped_img


def get_clothes_images(person):
    HEADERS = {'Authorization': f"token {TOKEN}", "Accept": "application/vnd.github.v3+json"}  
    
    clustersid = np.random.randint(0,9,3)

    if person.gender == "Male":
        return {"error":"We don't have clothes for you right now :c"}
    elif person.age <= 15:
        clustersid = np.random.randint(0,8,3)

    res = []
    for i in range(3):#3
        res.append(requests.get(f"{DATASET_URL}/{clustersid[i] }/{clustersid[i] }_{np.random.randint(0,50)}.png",
                                headers=HEADERS).json())

    changing_room  = save_recomendations(res)
    return changing_room

def save_recomendations(json):
    idx = 0
    changing_room =[]
    for cont in json:
        img_base64 = cont["content"]
        
        im_bytes = base64.b64decode(img_base64)

        im_arr = np.frombuffer(im_bytes, dtype=np.uint8)  
        img = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)

        cv2_img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        if not cv2.imwrite(os.path.join(os.path.dirname(__file__)+
        f"/changing_{idx}.png"), cv2_img):
            raise Exception("No directory")
                
        changing_room.append(os.path.join(os.path.dirname(__file__)+
            f"/changing_{idx}.png"))
        
        idx+=1

    return changing_room

def delete_background(img):
  
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

  # Se crea una máscara a partir de un humbral
  mask = cv2.threshold(gray, 250, 255, cv2.THRESH_BINARY)[1]

  # Se invierte la máscara
  mask = 255 - mask

  # Operaciones morfológicas para eliminar ruido
  kernel = np.ones((3,3), np.uint8)
  mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
  mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

  # Filtro gaussiano para suavizar
  mask = cv2.GaussianBlur(mask, (0,0), sigmaX=2, sigmaY=2, borderType = cv2.BORDER_DEFAULT)

  # Normalización
  mask = (2*(mask.astype(np.float32))-255.0).clip(0,255).astype(np.uint8)

  # Coloca la máscara en el canal alpha
  result = img.copy()
  result = cv2.cvtColor(result, cv2.COLOR_BGR2BGRA)
  result[:, :, 3] = mask

  return result

def put_clothes_on(person):

    # leer imagen original
    org_img = Image.open(person.photo)

    w, h = org_img.size
    # leer keypoints
    kps = person.keypoints

    shoulders_kps = kps[-2:]
    r_shoulder_kps = shoulders_kps[0]
    l_shoulder_kps = shoulders_kps[1]

    # calcular hombro der - hombro izq como el centro 
    chest_width = (r_shoulder_kps.x*w) - (l_shoulder_kps.x*w)
    middle_point_x = int(chest_width/ 2)


    # leer imagenes de ropa
    changing_room = person.changing_room

    recom_imgs=[]

    idx = 0
    for recom_path in changing_room:
        org_img = Image.open(person.photo)

        recom_img = cv2.imread(recom_path) #192 x 156
        recom_img = delete_background(recom_img)

        #Resize de la imagen dependiendo del tamaño del pecho
        cloth_width = recom_img.shape[1]
        scale_percent = chest_width*100 /  cloth_width

        width = int(recom_img.shape[1] * scale_percent * 1.5/ 100)
        height = int(recom_img.shape[0] * scale_percent *1.5 / 100)
        dim = (width, height)
        #resize
        recom_img = cv2.resize(recom_img, dim, interpolation = cv2.INTER_AREA)

        #Colocar recomendacion encima de la foto original
    
        alpha_s = recom_img[:, :, 3] / 255.0
        alpha_l = 1.0 - alpha_s
        x_offset= int(l_shoulder_kps.x*w)
        y_offset= int(l_shoulder_kps.y*h)

        cloth_on = org_img
        recom_img_pil = Image.fromarray(recom_img)

        cloth_on.paste(recom_img_pil, (x_offset, y_offset), recom_img_pil) 
        # cloth_on.show()

        #Guardar rutas
        cloth_on.save(os.path.join(os.path.dirname(__file__)+
        f"/recomendation_{idx}.png"))
                
        recom_imgs.append(os.path.join(os.path.dirname(__file__)+
            f"/recomendation_{idx}.png"))

        idx +=1

    return recom_imgs
    