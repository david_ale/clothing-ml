import os
from flask import request, Flask, send_from_directory, jsonify
from flask_cors import CORS, cross_origin


import cv2
import numpy as np


from utils.utils import allowed_file, cut_face, get_clothes_images, put_clothes_on
from person.person import Person
from models.keypoints import keypoints

import base64

app = Flask(__name__)
app.config["DEBUG"] = True
cors = CORS(app)

ALLOWED_EXTENSIONS = set([ 'data:image'])

person = Person()

# [API]
@app.route('/getClothes', methods=['POST'])
@cross_origin()
def getClothes():

    if request.method == 'POST':
        print("age" in request.json)
        if "age" not in request.json or "gender" not in request.json or "base64_img" not in request.json:
            return jsonify({"error": "Verifica la foto, la edad o el género"})
        else:
            #Carga el archivo
            img_file = request.json['base64_img']
            if not allowed_file(img_file, ALLOWED_EXTENSIONS):
                return {"error":"El tipo de archivo no es permitido"}
                
            im_bytes = base64.b64decode(img_file.split(",")[1])

            im_arr = np.frombuffer(im_bytes, dtype=np.uint8)  
            img = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)            

            #Guarda el archivo 
            if not cv2.imwrite(os.path.join(os.path.dirname(__file__),"./upload/person_photo.jpg"), img):
                raise Exception("No directory")

            #Asigna la photo al objeto person
            img_path = os.path.join(os.path.dirname(__file__)+"/upload/person_photo.jpg")
            person.setPhoto(img_path)

            #Calcula y asigna los keypoints
            calculated_keypoints = keypoints.calculate_keypoints(person.photo)
            person.setKeypoints(calculated_keypoints)

            if "error" in calculated_keypoints:
                return calculated_keypoints
            #Calcula y asigna la edad y el género
            #face_img = cut_face(person)

            #Get age, gender --------[NOT IMPLEMENTED]--------
            #age = getAge(face_img)
            #gender = getGender(face_img)
            req_age = int(request.json["age"])
            req_gender = str(request.json["gender"])
            person.setAge(req_age)
            person.setGender(req_gender)

            #Obtiene 3 imagenes de ropa dependiendo de la edad y el género
            changing_room = get_clothes_images(person)
            person.setChanging_Room(changing_room)

            if "error" in changing_room:
                return changing_room

            # Put clothes on (3 images)
            recommendations = put_clothes_on(person)
            person.setRecommendations(recommendations)


            response_imgs  = np.concatenate([person.changing_room, person.recomendations])
            response_urls = []
            for r_i in response_imgs:
                response_urls.append(f"/getImage/{r_i.split('/')[7]}") 
            
            #Return the 6 images, 3 clothes, 3 clothes_on
            return jsonify({"response":response_urls})

@app.route('/getImage/<path:path>', methods=['GET'])
def getImage(path):
    return send_from_directory('utils', path)

if __name__ == '__main__':
    app.run(debug=True)