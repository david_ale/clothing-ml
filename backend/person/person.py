
class Person():
    def __init__(self, keypoints=None, age=None, gender=None, photo=None, recommendations= None, changing_room=None, **kwargs):
        self.keypoints = keypoints
        self.age = age
        self.gender = gender
        self.photo = photo

    def setKeypoints(self, keypoints):
        self.keypoints = keypoints
    
    def setAge(self, age):
        if isinstance (age, int):
            self.age = age 
    
    def setGender(self, gender):
        if gender == "Male" or gender == "Female":
            self.gender = gender
    
    def setPhoto(self, photo):
        if photo:
            self.photo = photo  
    
    def setRecommendations(self, recomendations):
        self.recomendations = recomendations
    
    def setChanging_Room(self, changing_room):
        self.changing_room = changing_room