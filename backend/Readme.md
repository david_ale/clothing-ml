#  Backend

En esta carpeta se encuentran los archivos necesarios para desplegar la una api rest con flask

* models: Donde se encuentran los modelos entrenados listos para predecir
* person: Módulo que contiene la clase persona, usada para controlar  el estado de la aplicación
* requeriments: Donde se encuentran los archivos de requerimientos para diferentes entornos
* tests: Archivos de tests
* upload: Donde se guardan imagenes provenientes del api
* utils: Módulo que contiene diferentes funciones de utilidad

    
## Uso
**nota** python --version = 3.7.11
Para poder usar este proyecto en un ambiente de *desarrollo* se debe: 

*  `python app.py` Ejecutará un servicio flask que expondrá las url `/getClothes` y `/getImage`
*  `python -m unittest tests/{module_name}` Ejecutará los tests del módulo específico
